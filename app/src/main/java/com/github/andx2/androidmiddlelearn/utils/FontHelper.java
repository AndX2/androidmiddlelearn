package com.github.andx2.androidmiddlelearn.utils;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.util.Log;

import com.github.andx2.androidmiddlelearn.BuildConfig;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Andrew on 07.01.2017.
 */

public class FontHelper {

    private static final String DEBUG_TAG = "FontHelperTAG";
    private static final String FONTS_PATH = "fonts";

    public Map<String, Typeface> getTypefaceMap() {
        return typefaceMap;
    }

    private Map<String, Typeface> typefaceMap = new HashMap<>();

    public void init(Context context) {
        AssetManager assetManager = context.getAssets();
        try {
            String[] list = assetManager.list(FONTS_PATH);
            if (BuildConfig.DEBUG) {
                Log.d(DEBUG_TAG, list.length + "");
            }
            for (int i = 0; i < list.length; i++) {
                try {
                    if ((list[i].substring(list[i].length() - 4)).equalsIgnoreCase(".ttf")) {
                        String typefaceName = (list[i].substring(0, list[i].length() - 4));
                        typefaceMap.put(typefaceName, Typeface.createFromAsset(assetManager, FONTS_PATH + "/" + list[i]));
                        if (BuildConfig.DEBUG) {
                            Log.d(DEBUG_TAG, list[i]);
                        }
                    }

                } catch (Exception e) {
                    if (BuildConfig.DEBUG) {
                        Log.d(DEBUG_TAG, "error extracting extention font file");
                    }
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        if (BuildConfig.DEBUG) {
            Log.d(DEBUG_TAG, "typefaceMap size = " + String.valueOf(typefaceMap.size()));
        }

    }
}
