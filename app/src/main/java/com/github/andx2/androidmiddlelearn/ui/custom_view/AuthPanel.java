package com.github.andx2.androidmiddlelearn.ui.custom_view;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.github.andx2.androidmiddlelearn.R;

/**
 * Created by Andrew on 07.01.2017.
 */

public class AuthPanel extends LinearLayout {

    public static final String DEBUG_TAG = "AuthPanelTAG";
    public static final int LOGIN_STATE = 0;
    public static final int IDLE_STATE = 1;

    private int customState = IDLE_STATE;

    private EditText etEmail, etPass;
    private Button btnLogin, btnCatalog;
    private CardView cardView;


    public AuthPanel(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    // TODO: 07.01.2017 validate and save state of eMail and pass

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        etEmail = (EditText) findViewById(R.id.et_auth_login);
        etPass = (EditText) findViewById(R.id.et_auth_pass);
        cardView = (CardView) findViewById(R.id.auth_card);
        btnLogin = (Button) findViewById(R.id.btn_enter);
        btnCatalog = (Button) findViewById(R.id.btn_show_catalog);

        showViewfromState();
    }

    @Override
    protected Parcelable onSaveInstanceState() {
        Parcelable superState = super.onSaveInstanceState();
        SavedState savedState = new SavedState(superState);
        savedState.state = customState;
        return savedState;
    }

    @Override
    protected void onRestoreInstanceState(Parcelable state) {
        SavedState savedState = (SavedState) state;
        super.onRestoreInstanceState(state);
        setCustomState(savedState.state);
    }

    public void setCustomState(int customState) {
        this.customState = customState;
        showViewfromState();
    }

    private void showViewfromState() {
        if (customState == LOGIN_STATE) {
            showLoginState();
        } else {
            showIdleState();

        }
    }

    private void showIdleState() {
        cardView.setVisibility(GONE);
        btnCatalog.setVisibility(VISIBLE);
    }

    private void showLoginState() {
        cardView.setVisibility(VISIBLE);
        btnCatalog.setVisibility(GONE);
    }

    public String getEmailValue() {
        return String.valueOf(etEmail.getText());
    }

    public String getPassValue() {
        return String.valueOf(etPass.getText());
    }

    public boolean isIdle() {
        return (customState == IDLE_STATE);
    }

    static class SavedState extends BaseSavedState {

        private int state;

        public SavedState(Parcelable superState) {
            super(superState);
        }

        private SavedState(Parcel in) {
            super(in);
            state = in.readInt();
        }

        public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator<SavedState>() {

            @Override
            public SavedState createFromParcel(Parcel parcel) {
                return null;
            }

            @Override
            public SavedState[] newArray(int i) {
                return new SavedState[0];
            }
        };


        @Override
        public void writeToParcel(Parcel out, int flags) {
            super.writeToParcel(out, flags);
            out.writeInt(state);
        }
    }

}
