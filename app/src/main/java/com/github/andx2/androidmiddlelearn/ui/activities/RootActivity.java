package com.github.andx2.androidmiddlelearn.ui.activities;


import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.github.andx2.androidmiddlelearn.App;
import com.github.andx2.androidmiddlelearn.BuildConfig;
import com.github.andx2.androidmiddlelearn.R;
import com.github.andx2.androidmiddlelearn.mvp.presenters.AuthPresenter;
import com.github.andx2.androidmiddlelearn.mvp.presenters.IAuthPresenter;
import com.github.andx2.androidmiddlelearn.mvp.views.IAuthView;
import com.github.andx2.androidmiddlelearn.ui.custom_view.AuthPanel;

import java.util.Iterator;

public class RootActivity extends AppCompatActivity implements IAuthView, View.OnClickListener {

    AuthPresenter authPresenter = AuthPresenter.getInstance();

    private CoordinatorLayout coordinatorLayout;
    private Button btnShowCatalog, btnShowLogin;
    private CardView authCard;
    private AuthPanel authPanel;
    private TextView tvAppTitle;

    //region Life cycle methods

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_root);

        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinator_container);
        btnShowCatalog = (Button) findViewById(R.id.btn_show_catalog);
        btnShowCatalog.setOnClickListener(this);
        btnShowLogin = (Button) findViewById(R.id.btn_enter);
        btnShowLogin.setOnClickListener(this);
        authCard = (CardView) findViewById(R.id.auth_card);
        authPanel = (AuthPanel) findViewById(R.id.auth_wrapper);

        //region set AppTitle custom font from assets
        tvAppTitle = (TextView) findViewById(R.id.tv_app_title);
        Iterator<Typeface> iterator = App.getFontHelper().getTypefaceMap().values().iterator();
        if (iterator.hasNext()) {
            tvAppTitle.setTypeface(iterator.next());
        }
        //endregion

        authPresenter.initView();

    }

    @Override
    protected void onStart() {
        super.onStart();
        authPresenter.takeView(this);
    }

    @Override
    protected void onStop() {
        authPresenter.dropView();
        super.onStop();
    }

    //endregion

    //region IAuthView implementation
    @Override
    public void showMessage(String message) {
        Snackbar.make(coordinatorLayout, message, Snackbar.LENGTH_LONG).show();

    }

    @Override
    public void showError(Throwable e) {
        if (BuildConfig.DEBUG) {
            showMessage(e.getMessage());
            e.printStackTrace();
        } else {
            showMessage("Sorry, anything work with mistakes :(, try later.");
            // TODO: 06.01.2017 send error stacktrace to crashlytics
        }

    }

    @Override
    public void showLoad() {
        // TODO: 06.01.2017 show progress load
    }

    @Override
    public void hideLoad() {
        // TODO: 06.01.2017 hide load progress

    }

    @Override
    public IAuthPresenter getPresenter() {
        return authPresenter;
    }

    @Override
    public void showLoginBtn() {
        btnShowLogin.setVisibility(View.VISIBLE);

    }

    @Override
    public void hideLoginBtn() {
        btnShowLogin.setVisibility(View.GONE);

    }

  /*  @Override
    public void testShowLoginCard() {
        authCard.setVisibility(View.VISIBLE);
    }*/

    @Override
    public AuthPanel getAuthPanel() {
        return authPanel;
    }
    //endregion


    @Override
    public void onBackPressed() {
        if (!authPanel.isIdle()) {
            authPanel.setCustomState(AuthPanel.IDLE_STATE);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_show_catalog:
                authPresenter.clickOnShowCatalog();
                break;
            case R.id.btn_enter:
                authPresenter.clickOnLogin();
                break;
        }
    }
}
