package com.github.andx2.androidmiddlelearn.mvp.views;

import android.support.annotation.Nullable;

import com.github.andx2.androidmiddlelearn.mvp.presenters.IAuthPresenter;
import com.github.andx2.androidmiddlelearn.ui.custom_view.AuthPanel;

/**
 * Created by Andrew on 06.01.2017.
 */

public interface IAuthView {

    void showMessage(String message);

    void showError(Throwable e);

    void showLoad();

    void hideLoad();

    IAuthPresenter getPresenter();

    void showLoginBtn();

    void hideLoginBtn();


//    void testShowLoginCard();

    @Nullable
    AuthPanel getAuthPanel();
}
