package com.github.andx2.androidmiddlelearn.mvp.presenters;

import android.support.annotation.Nullable;

import com.github.andx2.androidmiddlelearn.mvp.views.IAuthView;

/**
 * Created by Andrew on 06.01.2017.
 */

public interface IAuthPresenter {

    void takeView(IAuthView view);

    void dropView();

    void initView();

    @Nullable
    IAuthView getView();

    void clickOnLogin();

    void clickOnFb();

    void clickOnVk();

    void clickOnTwitter();

    void clickOnShowCatalog();

    boolean checkUserAuth();


}
