package com.github.andx2.androidmiddlelearn;

import android.app.Application;

import com.github.andx2.androidmiddlelearn.utils.FontHelper;

/**
 * Created by Andrew on 07.01.2017.
 */

public class App extends Application {

    private static final String DEBUG_TAG = "AppTAG";
    private static FontHelper fontHelper = new FontHelper();


    @Override
    public void onCreate() {
        super.onCreate();
        fontHelper.init(getApplicationContext());
    }

    //region getters-setters
    public static FontHelper getFontHelper() {
        return fontHelper;
    }
    //endregion

}
