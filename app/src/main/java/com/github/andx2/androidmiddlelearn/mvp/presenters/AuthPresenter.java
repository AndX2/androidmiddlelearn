package com.github.andx2.androidmiddlelearn.mvp.presenters;

import android.support.annotation.Nullable;

import com.github.andx2.androidmiddlelearn.mvp.models.AuthModel;
import com.github.andx2.androidmiddlelearn.mvp.views.IAuthView;
import com.github.andx2.androidmiddlelearn.ui.custom_view.AuthPanel;

/**
 * Created by Andrew on 06.01.2017.
 */

public class AuthPresenter implements IAuthPresenter {

    private static AuthPresenter instance = new AuthPresenter();
    private AuthModel authModel;
    private IAuthView authView;

    private AuthPresenter() {
        authModel = new AuthModel();

    }

    public static synchronized AuthPresenter getInstance() {
        return instance;
    }

    @Override
    public void takeView(IAuthView view) {
        authView = view;
    }

    @Override
    public void dropView() {
        authView = null;
    }

    @Override
    public void initView() {

        if (getView() == null) return;
        if (checkUserAuth()) {
            getView().hideLoginBtn();
        } else {
            getView().showLoginBtn();
        }
    }

    @Nullable
    @Override
    public IAuthView getView() {
        return authView;
    }

    @Override
    public void clickOnLogin() {
        if (getView() != null && getView().getAuthPanel() != null) {
            if (getView().getAuthPanel().isIdle()) {
                getView().getAuthPanel().setCustomState(AuthPanel.LOGIN_STATE);
            } else {
                // TODO: 07.01.2017 auth user
                authModel.loginUser(getView().getAuthPanel().getEmailValue(), getView().getAuthPanel().getPassValue());
                getView().showMessage("request user auth");
            }
        }
    }

    @Override
    public void clickOnFb() {
        if (getView() == null) return;
        getView().showMessage("clickOnFb");

    }

    @Override
    public void clickOnVk() {
        if (getView() == null) return;
        getView().showMessage("clickOnVk");
    }

    @Override
    public void clickOnTwitter() {
        if (getView() == null) return;
        getView().showMessage("clickOnTwitter");
    }

    @Override
    public void clickOnShowCatalog() {
        if (getView() == null) return;
        getView().showMessage("Show catalog");

    }

    @Override
    public boolean checkUserAuth() {
        return authModel.isAuthUser();
    }

}
